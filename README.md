# This is a fork of my syslog-ng.conf file adapted to run with rsyslogd.

###rSyslog configuration.
###Originally brought together by buraglio@soltec.net 09/18/2000
###Modified again by buraglio@ncsa.edu ; cleaned up on 11/22/2004
###modified for CITES use by buraglio@illinois.edu 07/14/2009
###Ported to rsyslog for use in centos systems 12/2013
###Added stock config file for LibreNMS support, migrated to a modern configuration structure for Debuntu 5/21/2019
###Much referenced from http://www.howtoforge.com/building-a-central-loghost-on-centos-and-rhel-5-with-rsyslog
###It is freely available to use and repurpose with the ceveat that this file and the header in the rsyslog.conf file remain.
###Nick Buraglio
###http://www.forwardingplane.net

